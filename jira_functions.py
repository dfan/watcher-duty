import requests
import json
import re


# def read_bearer_token_from_file(file_path):
#     try:
#         with open(file_path, 'r') as file:
#             bearer_token = file.read().strip()
#             return bearer_token
#     except FileNotFoundError:
#         print("File not found. Make sure the file path is correct.")
#         return None

# token_file_path = 'jira_token.txt'
# jira_token = read_bearer_token_from_file(token_file_path)


# # TODO: read token from config
# headers = {
#     "Accept": "application/json",
#     "Authorization": f"Bearer {jira_token}" ,
# }


def get_tickets(jira_token: str):
    # TODO: the return part here doesn't look too nice...
    """This takes our Jira token and gets the current tickets in the backlog.

    Returns:
        ticket-name (str): {
          "stream": (int),
          "package": (str)
        }

    Example return:
      {
        "CS-1234": {
            "stream": "8",
            "package": "package_name",
        },
        "CS-5678": {
            "stream": "9",
            "package": "package_name",
        },
      }

    """

    headers = {
      "Accept": "application/json",
      "Authorization": f"Bearer {jira_token}" ,
    }

    url = "https://issues.redhat.com/rest/agile/1.0/board/13247/issue"

    print(headers)
    if not jira_token:
        print("Jira token not found.")
        return None

    # TODO: Query gets from the backlog, can change to to-do or whatever else if necessary
    jql_query = 'project = "CS" AND issuetype = "Task" AND status = "Backlog" AND text ~ "Allowlist the package"'

    params = {
        "jql": jql_query,
    }

    response = requests.request("GET", url, headers=headers, params=params)

    if response.status_code == 200:
        data = response.json()
        issues = data.get("issues", [])
        # Create a dictionary with "key" as the key and formatted information as the value
        # It will look something like this:
        # {
        #     "CS-1234": {
        #         "stream": "8",
        #         "package": "package_name",
        #     },
        #     "CS-5678": {
        #         "stream": "9",
        #         "package": "package_name",
        #     },
        # }
        issues_dict = {}
        for issue in issues:
            # This is the ticket number
            key = issue["key"]
            # This is the title of the ticket. We just want to get the package name and stream from this.
            summary = issue["fields"]["summary"]

            # Use regular expressions to extract stream, package and ticket number
            match = re.match(
                r"\[([^\]]+)\]\s+Allowlist the Package:\s+(.*?)\s+in", summary
            )


            # If the ticket is an "allowlist the package" ticket, it will add the stream and package to the dictionary
            if match:
                stream = match.group(1).strip()
                package = match.group(2).strip()

                # Extract the numeric part from the stream
                numeric_stream = re.search(r"\d+", stream)
                numeric_part = numeric_stream.group() if numeric_stream else None

                # Add the formatted information to the dictionary
                issues_dict[key] = {"stream": numeric_part, "package": package}

        return issues_dict
    else:
        print(f"Error: {response.status_code}, {response.text}")


# TODO: functions for story pointing ticket, assign, move ticket?
