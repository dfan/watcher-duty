import os
import requests
import ruamel.yaml
import pathlib

yaml = ruamel.yaml.YAML(typ="rt", pure=True)
yaml.preserve_quotes = True
yaml.indent(mapping=2, sequence=4, offset=2)

def add_to_zuul(package, key):
    """This uses the ruamel.yaml library to add our packages into the zuul configs.

    Args:
        package (str): The package to be added
        key (str): The ticket number
    """
    owd = os.getcwd()
    os.chdir(owd + "/project-config")

    # Pull from upstream in case there are changes
    os.system(
        "git remote add upstream git@gitlab.com:redhat/centos-stream/ci-cd/zuul/project-config.git"
    )
    os.system("git pull upstream master")

    fname = pathlib.Path("resources/centos-distgits.yaml")

    data_stream = open(fname, "r")
    data = yaml.load(data_stream)

    source_repositories = data['resources']['projects']['centos-distgits']['source-repositories']

    # The packages in the Zuul config are in this format
    new_entry = {
        f'redhat/centos-stream/rpms/{package}': {
            'default-branch': 'c9s'
        }
    }

    if new_entry not in source_repositories:
      source_repositories.append(new_entry)

    # Sort the file when finished and save it
    source_repositories.sort(key=lambda x: next(iter(x)))

    with open(fname, 'w') as yaml_file:
        yaml.dump(data, yaml_file)

    commit_to_git(package, key)
    os.chdir(owd)
    # push_to_gitlab()
    # create_mr_and_label()


def commit_to_git(package: str, key: str):
    """Commits the current changes to git

    Args:
        package (str): The package being added to the repo
        key (str): The ticket
    """

    os.system("git add resources/centos-distgits.yaml")
    # TODO: How will we handle the ticket number? have as input originally?
    os.system(f'git commit -m "Added {package} for {key}" --signoff')


def push_to_gitlab():
    owd = os.getcwd()
    os.chdir(owd + "/project-config")
    os.system(f"git push origin master")
    os.chdir(owd)


# TODO: not sure if we should keep this or manually do the MR ourselves to double check
def create_mr_and_label(package: str):
    """Creates a merge reqest to the config and adds the gateit label to it. The Zuul ci will run and merge once it is completed

    Args:
        package (str): The package to be added
    """
    with open(os.path.expanduser("~/.gitlab-token")) as thefile:
            mytoken = thefile.read().strip()
    # TODO: ticket number stuff again, update to proper branches here

    # Create the Merge Request
    data = {
        "id": 25494647,
        "source_branch": "master",
        "target_branch": f"add-{package}",
        "title": f"Added {package} for testticket",
        "labels": "gateit",
    }


    try:
        print("Creating merge request...")
        h = {"PRIVATE-TOKEN": mytoken, "Content-Type": "application/json"}
        resp = requests.post(
            f"https://gitlab.com/api/v4/projects/{id}/merge_requests",
            json=data,
            headers=h,
        )
        resp.raise_for_status()
    except:
        with open("errors.log", "a") as logfile:
            print(
                "Problem creating Merge Request in gitlab: {}\n{}".format(
                    package, resp.json()
                ),
                file=logfile,
            )

# This is probably isn't needed. I used this to find the packages that were in koji but not Zuul.
def compare_with_koji():
    # Extract repository names from Zuul config
    fname = pathlib.Path("project-config/resources/centos-distgits.yaml")

    data_stream = open(fname, "r")
    data = yaml.load(data_stream)

    zuul_repository_names = []
    source_repositories = data['resources']['projects']['centos-distgits']['source-repositories']
    for repository in source_repositories:
        # Extract the name after 'redhat/centos-stream/rpms/'
        name = list(repository.keys())[0].split('/')[3]
        zuul_repository_names.append(name)

    # Extract repository names from koji stream 10 config
    fname = pathlib.Path(f"koji-ops/tag-ansible/roles/stream10/vars/main.yml")

    data_stream = open(fname, "r")
    data = yaml.load(data_stream)

    koji_allowlist_pkgs = data["allowlist_pkgs"]

    # make a list of the packages that are in koji but not in zuul and add them to zuul
    missing_in_zuul = [item for item in koji_allowlist_pkgs if item not in zuul_repository_names]
    # print(missing_in_zuul)
    # print(len(missing_in_zuul))

    for package in missing_in_zuul:
      add_to_zuul(package)
