import json
import os
import webbrowser
import repo_functions
import koji_functions
import zuul_functions
import jira_functions

from dotenv import load_dotenv

# load .env file to environment
load_dotenv()

JIRA_TOKEN = os.getenv('JIRA_TOKEN')
KOJI_OPS_FORK = os.getenv('KOJI_OPS_FORK')
PROJECT_CONFIG_FORK = os.getenv('PROJECT_CONFIG_FORK')


# If the necessary repos don't exist we can clone them down
# TODO: have the forks come from a config?
def setup():
    # Gitlab Ops is used to create the repo and add necessary permissions
    os.system(
        "git clone git@gitlab.com:redhat/centos-stream/release-engineering/gitlab-ops.git"
    )
    # TODO: Change to actual koji ops repo instead of just testing
    os.system(f"git clone {KOJI_OPS_FORK}")
    # os.system(
    #     "git clone git@gitlab.com:redhat/centos-stream/ci-cd/zuul/project-config.git"
    # )
    os.system(f"git clone {PROJECT_CONFIG_FORK}")


def loop_through_tickets(tickets):
    # the key is the ticket number, eg CS-1234
    # the value contains both the package name and the stream number
    for key, value in tickets.items():
        # Create the repo - if it already exists this does nothing
        repo_functions.create_repo(value["package"], value["stream"])
        # # Add the packages to the koji allowlist
        koji_functions.add_to_koji_allowlist(value["package"], key, value["stream"])
        # # Add the packages to zuul
        zuul_functions.add_to_zuul(value["package"], key)
    # Push to your Gitlab fork once everything has been added
    koji_functions.push_to_gitlab()
    zuul_functions.push_to_gitlab()
      # # Cleanup the folders once every ticket has been dealt with
      # cleanup()


def cleanup():
    # Clean up the folders once we finish. This is to prevent us from pushing stuff we don't need to our repo
    os.system("rm -rf gitlab-ops")
    os.system("rm -rf koji-ops")
    os.system("rm -rf project-config")


if __name__ == "__main__":
    setup()
    tickets = jira_functions.get_tickets(JIRA_TOKEN)
    # print(tickets)
    loop_through_tickets(tickets)
    # cleanup()
