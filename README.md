# Watcher Duty
Information on the Watcher Duty process can be seen here: https://redhat.gitlab.io/centos-stream/infra/docs/watcher-duty/package/


## What does this do
The package addition process for CentOS Stream requires a few steps. Our team handles:
  * Creating the repo in Gitlab
  * Populating the repo based on the RHEL branch to enable sync
  * Adding the package to the koji allowlist
  * Adding the package to the Zuul config

This script looks at the CS Jira for the "Allowlist the package..." tickets. It will then create and populate the Gitlab repo, update the Koji allowlist and update the Zuul config.
It will then push to your forks of this so you can make the appropriate Merge Requests.

## Prerequisites
* An API-scoped Gitlab token:
  * Log in using SSO
  * Click on your avatar in the top-right corner
  * Visit Preferences => Access Tokens
  * Add a personal access token, with a reasonable expiration, and the ‘api’ scope
  * Copy this string into `~/.gitlab-token` on your workstation and make sure to `chmod 0600 ~/.gitlab-token`
  * Once this is done, I recommend you clone the [gitlab-ops](https://gitlab.com/redhat/centos-stream/release-engineering/gitlab-ops) repo and to rename `python-gitlab.cfg.example` to `python-gitlab.cfg` and to add your private token to there.

Provided is a `.env.example` file. Be sure to copy this and create your own `.env` file to populate. You will need to add stuff into this.

* A working fork of [koji-ops](https://gitlab.com/redhat/centos-stream/release-engineering/koji-ops)
  * Make sure to add the clone url into your `.env` file, eg `KOJI_OPS_FORK=git@gitlab.com:dfan/koji-ops.git`

* A working fork of the [Zuul project config](https://gitlab.com/redhat/centos-stream/ci-cd/zuul/project-config)
  * Make sure to add the clone url into your `.env` file, eg `PROJECT_CONFIG_FORK=git@gitlab.com:dfan/project-config.git`

* A Jira token
  * Navigate to your Jira profile. On the left you can create a personal access token. Create this and add it into your `.env` as `JIRA_TOKEN=<your token here>`

## Setup
Personally I like to use a virtual env with this command
```
python3 -m venv venv
source venv/bin/activate
```

The current only dependency for this project is `ruamel-yaml`. If this is installed on the system you won't need to use poetry. With a future update I may migrate to use the Jira package.

This project uses poetry. To install the dependencies type:
```
poetry install
```

Currently, the script will take all of the tickets with the "Allowlist the ..." in the CS backlog in Jira and will handle them. This goes on the assumption that the appropriate branches exist in the RHEL dist git.

To run this, you should move the tickets you want to process into the backlog. You can then run:

```
python3 main.py
```

This will create and populate the Gitlab repo and set the appropriate permissions. It will then add the packages into the appropriate parts of your `koji-ops` and `project-config` forks.

You should now navigate to your forks and create merge requests into the upstream repos. In the case of `koji-ops` the pipeline will run once the change is merged. For `project-config` be sure to add the `gateit` label and the bot should merge your change.

