# from ruamel.yaml import YAML
import os
import time
import requests
import ruamel.yaml
import pathlib


yaml = ruamel.yaml.YAML(typ="rt", pure=True)
yaml.preserve_quotes = True
yaml.indent(mapping=2, sequence=4, offset=2)


def add_to_koji_allowlist(package: str, ticket: str, stream: int):
    """This uses the ruamel.yaml library to add our packages into the appropriate configs.
    Once a package has been added, it will make a commit appropriate to the package and stream.
    After all packages are added, it will push to our koji-ops fork, ready for a merge request.

    Args:
        packages (list[str]): The list of packages that are to be added
        tickets (list[str]): The list of tickets that match with the packages
        stream (int): the stream that we want (8, 9, 10)
    """
    owd = os.getcwd()
    os.chdir(owd + "/koji-ops")

    # Pull from upstream in case there are changes
    os.system(
        "git remote add upstream git@gitlab.com:redhat/centos-stream/release-engineering/koji-ops.git"
    )
    os.system("git pull upstream main")

    fname = pathlib.Path(f"tag-ansible/roles/stream{stream}/vars/main.yml")

    data_stream = open(fname, "r")
    data = yaml.load(data_stream)

    if package in data["allowlist_pkgs"]:
        print(f"nothing to do {package} is already in allowlist_pkgs")
    else:
        data["allowlist_pkgs"].append(package)
        data["allowlist_pkgs"].sort()
        with open(fname, "w") as yaml_file:
            yaml.dump(
                data,
                yaml_file,
            )
    commit_to_git(package, ticket, stream)
    os.chdir(owd)


def commit_to_git(package: str, ticket: str, stream: int):
    """Commits the current changes to git

    Args:
        package (str): The package being added to the repo
        ticket (str): The ticket that the package is being added for
        stream (int): The stream it is being added to
    """
    os.system("git add " + f"tag-ansible/roles/stream{stream}/vars/main.yml")
    os.system(f'git commit -m "Added {package} for {ticket}" --signoff')


# Update this for multiple packages + figure out commits
def push_to_gitlab():
    owd = os.getcwd()
    os.chdir(owd + "/koji-ops")
    os.system("git push origin main")
    os.chdir(owd)


# This might end up running outside of our machine
def run_playbook(stream: int):
    """Runs the Ansible playbook. Requires kerberos ticket first.

    Args:
        stream (int): The stream we want (8, 9, 10)
    """
    # os.system(f'KOJI_PROFILE=streaminternal ansible-playbook playbooks/do-tags.yml -i hosts -e "stream=stream{stream}"')


# Create MR https://docs.gitlab.com/ee/api/merge_requests.html#create-mr
def merge_to_repo(packages: list[str], tickets: list[str]):
    """This function will create a merge request with koji-ops and merge it.

    Args:
        packages (list[str]): The list of packages we are adding
        tickets (list[str]): The list of tickets relevant to the packages
    """

    # TODO: update this, currently its my personal version of koji-ops. change to the actual version. add to readme where people should get the id for their fork
    id = 35139867
    # id = 24194572

    with open(os.path.expanduser("~/.gitlab-token")) as thefile:
        mytoken = thefile.read().strip()
    # TODO: ticket number stuff again, update to proper branches here

    packages_string = ""
    for package in packages:
        packages_string += f"{package}, "

    tickets_string = ""
    for ticket in tickets:
        tickets_string += f"{ticket}, "

    # Create the Merge Request
    data = {
        "id": id,
        "source_branch": "test2",
        "target_branch": "test1",
        "title": f"Added {packages_string} for {tickets_string}",
    }

    try:
        print("Creating merge request...")
        h = {"PRIVATE-TOKEN": mytoken, "Content-Type": "application/json"}
        resp = requests.post(
            f"https://gitlab.com/api/v4/projects/{id}/merge_requests",
            json=data,
            headers=h,
        )
        resp.raise_for_status()
    except:
        with open("errors.log", "a") as logfile:
            print(
                "Problem creating Merge Request in gitlab: {}\n{}".format(
                    packages, resp.json()
                ),
                file=logfile,
            )

    # Wait a few seconds for gitlab to actually make the branch...
    time.sleep(3)

    # Merge it
    merge_request_iid = resp.json().get("iid")
    merge_data = {
        "id": id,
        "merge_request_iid": merge_request_iid,
        "should_remove_source_branch": True,
        "state": "merged",
    }
    try:
        print("Attempting to merge...")
        h = {"PRIVATE-TOKEN": mytoken, "Content-Type": "application/json"}
        resp = requests.put(
            f"https://gitlab.com/api/v4/projects/{id}/merge_requests/{merge_request_iid}/merge",
            json=merge_data,
            headers=h,
        )
        print(resp.content)
        resp.raise_for_status()
    except:
        with open("errors.log", "a") as logfile:
            print(
                "Problem merging Merge Request in gitlab: {}\n{}".format(
                    packages, resp.json()
                ),
                file=logfile,
            )
