import os
import webbrowser


def create_repo(repo: str, stream: int):
    """Creates and populates new repos in Gitlab for the respective stream. It populates the branch with the contents of the
    current branch in RHEL git. It then will run the group editor to make sure the correct permissions are applied.

    Args:
        repos (list[str]): The list of repos that are to be created
        stream (int): the stream that we want (8, 9)
    """
    owd = os.getcwd()
    # Create the repo
    os.chdir(owd + "/gitlab-ops/rhel9-package-seed/")
    os.system("./new-gitlab-repo " + repo)
    os.chdir(owd)
    # os.system("./gitlab-ops/rhel9-package-seed/new-gitlab-repo " + repo)

    # TODO: probably dont need this, can keep for preference
    webbrowser.open("https://gitlab.com/redhat/centos-stream/rpms/" + repo)

    # Clone and populate the branches
    os.system("git clone git@gitlab.com:redhat/centos-stream/rpms/" + repo + ".git")
    os.chdir(repo)
    os.system("pwd")
    os.system("git remote add rhel https://pkgs.devel.redhat.com/git/rpms/" + repo)

    os.system(f"git switch --orphan c{stream}s")
    os.system(f"git pull rhel rhel-{stream}-main")
    os.system(f"git push --set-upstream origin c{stream}s")

    os.chdir(owd)
    # Remove the repo
    os.system("rm -rf " + repo)
    # Protect the branch
    os.chdir(owd + "/gitlab-ops/group_editor")
    os.system(
        "python3 gitlab_group_editor.py --cs_setup --project redhat/centos-stream/rpms/"
        + repo
    )
    os.chdir(owd)


