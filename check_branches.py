import subprocess

# TODO: maybe have this get the stuff from jira and then check
def check_branch_existence(repo_url, branch_name):
    remote = "https://gitlab.com/redhat/centos-stream/rpms/" + repo_url
    # remote = "https://pkgs.devel.redhat.com/cgit/rpms/" + repo_url
    try:
        # Run the git ls-remote command to check if the branch exists
        subprocess.run(
            ["git", "ls-remote", "--heads", remote, branch_name],
            check=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        return True
    except subprocess.CalledProcessError:
        return False

target_branch = "rhel-9-main"

for repo_url in stream9repos:
    if check_branch_existence(repo_url, target_branch):
        print(f"Branch '{target_branch}' exists in {repo_url}")
    else:
        print(f"Branch '{target_branch}' does not exist in {repo_url}")
